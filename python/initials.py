#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" create TeX input file with three letter codes for KKM members """

from collector import Collector
import logging

c = Collector('mailer.ini')

print(r"\section*{Abkürzungsliste}\small")
abbrevs = {}
for voice in ['SOPRAN', 'ALT', 'TENOR', 'BASS']:
    singers = c.get_list(voice)
    print(fr"\textbf{{{voice.capitalize()}}}", end=" ")
    for m in sorted(singers):
        fullname = f"{m.firstname} {m.name}"
        abbrev = m.firstname[0:2] + m.name[0:1]
        if abbrev in abbrevs.keys():
            logging.warn(f"{abbrev} not unique ({abbrevs[abbrev]}, {fullname})")
        else:
            abbrevs[abbrev] = fullname
        print(f"{fullname}: {abbrev}", end=", ")
    print(r"\par")
