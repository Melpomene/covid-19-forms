#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" Create form in portable document format """

from datetime import datetime, timedelta
from pylatex import Document, Command
from pylatex.utils import NoEscape

def next_thursday(date=None):
    if date is None:
        date = datetime.today()
    w = date.weekday()
    return date + timedelta(days=(3 - w)%7)

class KKMForm(Document):
    def __init__(self, attendee, email, phone, location=None, date=None):
        super().__init__(documentclass='covid19form', document_options='ngerman', inputenc=None)

        thursday = next_thursday(date)
        self.preamble.append(NoEscape(rf'\setdatenumber{{{thursday.year}}}{{{thursday.month}}}{{{thursday.day}}}'))
        # self.preamble.append(NoEscape(r'\setdatenumber{2020}{9}{20}'))
        self.preamble.append(Command('attendee', attendee))
        self.preamble.append(Command('email', email))
        self.preamble.append(Command('phone', phone))
        if location:
            self.preamble.append(Command('location', location))
        
        self.append(NoEscape(r'\makeform'))
        self.append(NoEscape(r'\HygieneConcept'))
        self.append(NoEscape(r'\Abbreviations'))

    def __str__(self):
        return self.dumps()

if __name__ == '__main__':
    from faker import Faker
    import random
    
    today = datetime.today()
    person   = Faker('de_DE')
    attendee = person.name()
    email    = person.email()
    phone    = person.phone_number()
    location = random.choice(["Schiff", "Turmsaal", "M1", None])
    
    form = KKMForm(attendee, email, phone, location)
    form.generate_pdf('teilnahme', compiler='xelatex')
