#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" mail COVID-19 rehearsal participation forms """

from time import sleep

from kkmform import KKMForm
from configurator import KKMConfig
from collector import Collector
# the collector reads the configuration file. That is bad.
# We need a Configurator

import email, smtplib, ssl
from email import encoders
from email.header import Header
from email.utils import formataddr
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import logging
from datetime import date
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                    filename=date.today().strftime("%Y-%m-%d.log"),
                    level=logging.DEBUG)

import argparse
parser = argparse.ArgumentParser(description='Create and send personalized COVID-19 forms')
parser.add_argument('-a', '--all', action='store_true',
                    help='created forms for all users')
parser.add_argument('-b', '--body', nargs='?', default="Viele Grüße\n Richard", 
                    help='use BODY as message body')
parser.add_argument('-s', '--send', action='store_true', 
                    help='send generated PDFs to users')
parser.add_argument('-t', '--test', action='store_true', 
                    help='send generated PDFs to richard.hirsch@vfemail.net')
parser.add_argument('-u', '--user', action='append',
                    help='create forms for USER') 
parser.add_argument('--version', action='version', version='1.0')
args = parser.parse_args()

config = KKMConfig('mailer.ini')

smtp_server = config['SMTP']['SERVER']
port = config['SMTP']['SSL_PORT']
user = config['SMTP']['USER']
password = config['SMTP']['PASSWORD']
context = ssl.create_default_context()

locations = {'S': 'KS', 'A': 'KS', 'T': 'KT', 'B': 'M1'}

def location(user):
    return locations[user.voice[0]]

def create_form(user):
    attendee = f"{user.firstname} {user.name}"

    form = KKMForm(attendee, user.email, user.phone, location=location(user))
    basename = user.user
    form.generate_pdf(basename, compiler='xelatex')
    logging.debug(f"{basename}.pdf created")

def send_mail(m, to=None, subject="Teilnahmeerklärung für Präsenzprobe"):
    # generate attachment
    with open(f"{m.user}.pdf", "rb") as attachment:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())
    encoders.encode_base64(part)
    part.add_header(
        "Content-Disposition",
        f"attachment; filename= {m.user}.pdf",
    )

    # create message
    message = MIMEMultipart()
    message["To"] = to if to else m.email
    message["From"] = formataddr((str(Header('Konkordien-Kantorei Mannheim', 'utf-8')), user))
    message["Subject"] = subject
    message.attach(MIMEText(body, "plain"))
    message.attach(part)

    # send message
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(user, password)
        server.sendmail(message['From'], message['To'], message.as_string() )
        logging.info(f"message sent to {message['To']}")


body = """Liebe Konkordianerinnen und Konkordianer, 

hier die Teilnahmeerklärungen für nächsten Donnerstag. 

Es gibt gute Nachrichten: Die Leitung des Freiburger Instituts für
Musikermedizin (wusstet Ihr, dass es das gibt? ich nicht) hat die
Abstandsempfehlungen an den Stand der Wissenschaft zur Aerosol- und
Tröpchenübertragung angepasst. Damit gelten auch nach vorne 2 Meter
Abstand als ausreichend. (Wissenschaftler aus Heilbronn und München
empfehlen allerdings 2,5 Meter Abstand nach vorne.) Die Regeln auf
Seite 2 sind entsprechend aktualisiert.

(Neben dem Abstand, dem Verwenden des Mund-Nasenschutzes, dem großen
Raum mit regelmäßiger Durchlüftung ist die sogenannte In-coming
Kontrolle noch ein wesentlicher Aspekt zur Risikoreduktion. Damit ist
gemeint, dass wir nicht zur Probe kommen, wenn es einen Verdacht auf
Ansteckungsfähigkeit gibt. Aber das steht ja auch auf den
Teilnahmeerklärungen.)

Die Aktualisierungen für die Telefonnummern, die mich erreicht haben,
habe ich hoffentlich alle eingearbeitet (bitte noch einmal
kontrollieren). Nächste Woche sind dann die Mobilnummern dran.

Viele Grüße
Richard

PS. Für alle, die es genau wissen wollen, hier der Link zur Gefährdungsbeurteilung:
https://www.mh-freiburg.de/hochschule/covid-19-corona/risikoeinschaetzung

- Bewertung der Tröpchenübertragung bei Gesang: Seite 20 und 23
- Bewertung der Übertragung durch Aerosole: Seite 21f
- drei Säulen der Risikoreduktion im Musikbereich: Seite 11
"""

c = Collector(config)
recipients = c.get_list('CHOR')
for member in recipients:
    if member.email == "":
        logging.debug(f"{member.firstname} {member.name} skipped")
        continue
    if args.user and member.user not in args.user:
        logging.debug(f"{member.firstname} {member.name} skipped")
        continue

    create_form(member)
    if args.send:
        send_mail(member, to='richard.hirsch@vfemail.net' if args.test else None)
        sleep(2)
