#! /usr/bin/env python

""" configurator.py
    read configuration file for KKM mailings
"""
from configparser import ConfigParser, NoOptionError, NoSectionError

import glob, re
import toml

import logging

class KKMConfig(ConfigParser):
    def __init__(self, config_file):
        super().__init__()
        self.read(config_file)
        self.logger = logging.getLogger('configurator')
        self.logger.setLevel(self['APP']['LOGLEVEL'])
        self.logger.info(f'read config file {config_file}')

