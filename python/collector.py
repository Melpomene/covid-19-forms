#! /usr/bin/env python

""" collector.py
    collect E-Mail addresses of the members of Konkordien-Kantorei Mannheim
    should be run every time an email msessage is sent
"""

import glob, re
import logging
import toml

class Member():
    def __init__(self, userfile):
        record =  toml.loads( userfile.read() )
        self.user = record['user']
        self.name = record['name']
        self.firstname = record['firstname']
        self.email = record['email']
        if 'phone' in record and record['phone']:
            self.phone = record['phone']
        elif 'mobile' in record:
            self.phone = record['mobile']
        else:
            self.phone = ""
        self.voice = record['voice']
        self.board = record.get('board', False)
        self.board_ext = record.get('board_ext', False)
        self.childcare = record.get('childcare', False)

    def __eq__(self, other):
        return self.name == other.name and self.firstname == other.firstname
        
    def __lt__(self, other):
        if self.name == other.name:
            return self.firstname < other.firstname
        else:
            return self.name < other.name

    def full_name(self):
        return '{name}, {firstname}'.format(**self.__dict__)

    
class Collector():
    def __init__(self, config):
        self.config = config
        self.logger = logging.getLogger('collector')
        self.logger.setLevel(self.config['APP']['LOGLEVEL'])

        self.srcdir = self.config['APP']['DATADIR']
        self.lists = self.config['LISTS']
        self.members = []
        for u in glob.glob(self.srcdir + '/*.toml'):
            with open(u, 'r') as fh:
                self.members.append(Member(fh))
        self.logger.info(f'data of {len(self.members)} members parsed.')

    def get_list(self, lname):
        if lname in self.lists.keys():
            members = list(filter(lambda m: eval(self.lists[lname]), self.members))
            self.logger.info(f'sending {len(members)} recipients for list "{lname}".')
            return members
        else:
            self.logger.warning(f'List "{lname}" not found!')
            return None
