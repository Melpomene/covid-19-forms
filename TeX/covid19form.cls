\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{covid19form}[2020/07/08 KKM COVID-19 forms LaTeX class]

\LoadClass[12pt, a4paper]{article}

\RequirePackage[T1]{fontenc}
\RequirePackage{parskip}
\RequirePackage{datenumber}

%% Font
\RequirePackage{helvet}
\renewcommand{\familydefault}{\sfdefault}

%% Language
\RequirePackage{polyglossia}
\setmainlanguage{german}

%% Logo
\RequirePackage{graphicx}
\newlength{\@logoheight}\setlength{\@logoheight}{10mm}

%% Page layout
\RequirePackage[left=2cm,right=1.5cm,top=41mm,bottom=10mm,headheight=50pt,headsep=20mm]{geometry}
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lhead{\includegraphics[height=\@logoheight]{../TeX/logo.jpg}}
\chead{\thebody}

\setlength{\unitlength}{1mm}
\setlength{\parindent}{0pt}

%% Title
\def\theformname{Teilnahmeerklärung}
\providecommand{\formname}[1]{\def\theformname{#1}}

\def\thelogo{\THlogo}
\providecommand{\logo}[1]{\def\thelogo{#1}}

\def\thebody{Konkordien-Kantorei Mannheim}
\providecommand{\body}[1]{\def\thebody{#1}}

\def\theattendee{Richard Hirsch}
\providecommand{\attendee}[1]{\def\theattendee{#1}}

\def\theemail{}
\providecommand{\email}[1]{\def\theemail{\texttt{#1}}}

\def\thephone{}
\providecommand{\phone}[1]{\def\thephone{#1}}

\def\thelocation{}
\providecommand{\location}[1]{\def\thelocation{#1}}

% form widges
\RequirePackage{amssymb}
\newcommand{\@checkbox}[1]{$\Large\square$\hspace{0.5em}#1}
\newcommand{\@timeform}[1]{%
  \makebox[40mm][l]{#1}\strut%
  \qquad\makebox[8mm]{\hrulefill} : \makebox[8mm]{\hrulefill}\quad Uhr}

% Section
\newcommand{\@section}[1]{\par\bigskip\hrule\par\smallskip\textbf{\normalsize#1}\par\smallskip}

% Items
\def\labelitemi{$\square$}

\newcommand{\makeform}{%
  \vspace*{-20mm}
  \textbf{\large \theformname}\par
  \hfill%
  \begin{tabular}{@{}ll@{}}
    Datum:          & \textbf{\datedate} \\
    Name:           & \theattendee \\
    E-Mail-Adresse: & \theemail \\
    Telefonnummer:  & \thephone
  \end{tabular}
  %
  \@section{vor Beginn der Probe auszufüllen:}
  \@timeform{Ankunft im Gebäude:} %
  \hfill%
  \begin{tabular}[t]{@{}l@{}}
    \@checkbox{Konkordienkirche – Hauptschiff} \\
    \@checkbox{Konkordienkirche – Turmsaal}    \\
    \@checkbox{Haus der Evangelischen Kirche M1\thinspace 1a}
  \end{tabular}%
  \par\bigskip%
  \begin{itemize}
  \item Ich nehme am gemeinsamen Musizieren auf eigene Verantwortung teil.\newline
    Die Verhaltensregeln auf der Rückseite sind mir bekannt.
  \item Ich hatte nach meiner Kenntnis in den vergangenen 14~Tagen
    keinen Kontakt zu einer mit dem neuartigen Coronavirus SARS-CoV-2 infizierten
    Person.
  \item Es liegen nach meiner Kenntnis kein Gründe für häusliche
    Selbstisolation oder Quarantäne bei mir vor.
  \item Ich fühle mich gesund und habe keine erkältungsähnlichen
    Symptome.
  \end{itemize}
  % 
  \@section{nach der Probe auszufüllen:}
  \@timeform{Verlassen des Gebäudes:} %

  Beim Musizieren waren meine unmittelbaren Nachbarn (Abkürzungen auf der Rückseite):
  \begin{center}
    \begin{picture}(100, 42)(-50,-18)
      \put(-10,0){\put(0,0){\line(1,0){20}}\put(0,-3){\scriptsize Platznummer}}
      \put(-10, 15){\put(0,0){\line(1,0){20}}\put(0,-2){\scriptsize vorne}}
      \put(-10,-15){\put(0,0){\line(1,0){20}}\put(0,-2){\scriptsize hinten}}
      \put(-60, 0){\put(0,0){\line(1,0){20}}\put(0,-2){\scriptsize links}}
      \put( 40, 0){\put(0,0){\line(1,0){20}}\put(0,-2){\scriptsize rechts}}
    \end{picture}
  \end{center}
  \hrule
  Besondere Vorkommnisse:
  % \hfill\parbox{10cm}{\footnotesize(z.\thinspace B.
  %   Abstand ohne Mund-Nasen-Bedeckung $<$ \mbox{2\thinspace m},
  %   Abstand mit Mund-Nasen-Bedeckung $<$ \mbox{1,5\thinspace m}, u.\thinspace ä.)}
  \par\vspace*{-1.5ex}{\footnotesize(z.\thinspace B.
    Abstand ohne Mund-Nasen-Bedeckung $<$ \mbox{2\thinspace m},
    Abstand mit Mund-Nasen-Bedeckung $<$ \mbox{1,5\thinspace m}, u.\thinspace ä.)}
  \par\strut\hrule\par\medskip\strut\hrule\par\bigskip
  ggf. Rückmeldung zur Organisation:\quad
  \par\strut\hrule\par\medskip\strut\hrule\par\bigskip
  \par\bigskip
  \begin{picture}(100,7)(0, -3)
    \put(0, 0){\line(1, 0){60}}
    \put(0, -3){\scriptsize Unterschrift}
  \end{picture}
  \newpage
}
\newcommand{\HygieneConcept}{%
  \section*{Hygienekonzept}
  \begin{enumerate}
  \item In alle Richtungen \textbf{mind. 2~Meter Abstand} halten (nach
    vorne besser 2,5~Meter).
  \item Händedesinfektion bitte benutzen.
  \item Geht direkt zu Eurem Platz. Eine Nummer habt Ihr vor der Probe
    über die Stimmsprecher*innen bekommen.
  \item Mund-Nasen-Bedeckung bis zum Stuhl tragen.
  \item Maske auch in den Pausen tragen.
  \item Chormitglieder tragen keine Stühle zurück, das machen die Stimmsprecher*innen. 
  \item Immer bei geöffnetem Fenster proben.
  \item Probe nach 45~Minuten beenden.
  \item Alle müssen den von den Stimmsprechern versendeten Zettel
    ausfüllen. Es darf niemand gehen, ohne diesen Zettel ausgefüllt zu
    haben!
  \item Es dürfen keine Menschentrauben entstehen. Bitte achtet auf die
    \textbf{Abstandsregeln} -- danke!
  \end{enumerate}
}
\newcommand{\Abbreviations}{%
  \section*{Abkürzungsliste}\small
  \textbf{Sopran:} Almuth Bedenbender: AlB, Petra Born: PeB, Vanessa
  Buffy: VaB, Stefanie Dunz: StD, Christiane Erzgräber: ChE, Linda
  Fehrs: LiF, Ingrid Fellner: InF, Clara Isabel Frank: ClF, Silke
  Hecht: SiH, Judith Hemmerling: JuH, Andrea Imle: AnI, Anita Kennard:
  AnK, Heike Kiefner-Jesatko: HeK, Ellen Krüger: ElK, Gabriele
  Mathieu-Hörcher: GaM, Susanne Matzker: SuM, Annette Meyer: AnM,
  Silke Müller: SiM, Noemi Rückert: NoR, Julia Scharfenstein: JuS,
  Monika Schäfer-Romanski: MoS, Annette Schönfeldt: AnS, Susanne
  Sieben: SuS, Isabel Sohn-Frank: IsS, Fiona Stadler: FiS, Urte
  Thulin: UrT, Mireille Vogt: MiV, Mareike Walther: MaW, Constanze
  Woltag: CoW\par
  \textbf{Alt:} Ursula Ascheberg: UrA, Christine Beers: ChB, Uta
  Bischoff-Peters: UtB, Kaja von Campenhausen: KaC, Aude Dettlinger:
  AuD, Claudia Dörr: ClD, Maike Dörries: MaD, Anne Fabian-Puchinger:
  AnF, Petra Fragner: PeF, Dania Graf: DaG, Ulrike von der Heidt: UlH,
  Erdmuth Hempel: ErH, Regine Heuchert: ReH, Angela Keller: AnK,
  Elisabeth Kleber: ElK, Martina Köster: MaK, Angelika Moritz: AnM,
  Hanni Neubauer: HaN, Nadja Peter: NaP, Beate Sandler: BeS, Anja
  Schneeweiß: AnS, Eva Schüßler: EvS, Helena Seifried: HeS, Christine
  Stadler-Neckerauer: ChS, Gabriele Wessel: GaW, Mirjam Wiesenfeldt:
  MiW\par
  \textbf{Tenor:} Harald von Campenhausen: HaC, Richard Hirsch: RiH,
  Jörg Laubersheimer: JöL, Ralf Lindenbach: RaL, Philipp Meixner: PhM,
  Bertold Planer-Friedrich: BeP, Alexander von Reden: AlR, Ellen
  Richter-Oesterreich: ElR, Alexander Sauter: AlS, Markus Schäfer:
  MaS, Wolfram Siefke: WoS, Bernhard Stang: BeS, Jürgen Thölke: JüT,
  Wolfgang Westerhaus: WoW\par
  \textbf{Bass:} Michael Black: MiB, Malte Brakebusch: MaB, Ulrich
  Brink: UlB, Stefan Dettlinger: StD, Jürgen Frank: JüF, Jörg
  Friedrich: JöF, Michael Gräf: MiG, Walter Götzger: WaG, Philipp
  Hasper: PhH, Gregor Haverkemper: GrH, Hanscarl Heise: HaH,
  Ulrich Hörcher: UlH, Michael Imle: MiI, Günther
  Krysmanski: GüK, Mischa Kurth: MiK, Ulrich Nowak: UlN, Christian
  Peters: ChP, Jens Riehle: JeR, Armin Schwarz: ArS, Johannes Vogt:
  JoV
} \endinput
% Local Variables:
% TeX-master: "teilnahme.tex"
% TeX-engine: xetex
% End:
