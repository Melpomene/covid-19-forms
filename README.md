[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)

# Musizieren während der Corona-Pandemie

Der Hauptübertragungsweg für das neuartige [Coronavirus
SARS-CoV-2](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/nCoV_node.html)
ist „die respiratorische Aufnahme virushaltiger Flüssigkeitspartike,
die beim Atmen, Husten, Sprechen und Niesen entstehen“ ([SARS-CoV-2
Steckbrief, Robert Koch Institut,
2020](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Steckbrief.html#doc13776792bodyText1)). Dadurch wird gemeinsames Musizieren zum Problem – insbesondere das gemeinsame Singen.

Durch Verzögerungen bei der Signalübertragung bleibt das Musizieren
über eine Internetverbindung unbefriedigend; und Präsenzproben
unterliegen wegen des hohen Infektionsrisikos
natürlich auch entsprechend hohen Auflagen.

Eine davon ist das **Nachhalten der Kontakte**. 

Dieses Formular dient dazu, die Kontakte beim Proben der
[Konkordien-Kantorei Mannheim](http://www.musik-an-konkordien.de/) nachzuhalten.

![Formular](TeX/teilnahme-0.png)
![Hygienekonzept](TeX/teilnahme-1.png)


## Motivation

Seit Juli 2019 verschickt ein Python-Programm  die Informationen der 
Chorleitung und des Vorstandes an die Sängerinnen und Sänger der
Konkordien-Kantorei.

Im Juli 2020 mussten an der Fakultät für Angewandte
Naturwissenschaften der TH Köln prüfungsbezogen [Formulare für
eidesstattliche
Erklärungen](https://gitlab.com/thk1/statutory-declaration) generiert
werden. Diese Formulare wurden mit Hilfe der Python-Bibliothek
[PyLaTeX](https://jeltef.github.io/PyLaTeX/current/) und dem Programm
XeLaTeX im [*portable document* Format](https://en.wikipedia.org/wiki/PDF) erzeugt.

Es lag nun nahe, die beiden Ansätze zu vereinigen und personenbezogene
Formulare für die Chormitglieder mit PyLaTeX aus der Mitgliederdatenbank zu erzeugen und 
über das Mailprogramm zu verschicken.

## Formular

Das Formular wird von [LaTeX](https://www.latex-project.org/) aus dem
Datum, Namen, E-Mail-Adresse, Telefonnummer und ggf. Probenort
generiert und von [XeTeX](https://en.wikipedia.org/wiki/XeTeX) im
*portable document* Format gesetzt.

```
\documentclass{covid19form}

\day = 9
\attendee{Walter Radisch}
\email{wradisch@margraf.de}
\phone{+49(0) 835274561}
\location{M1}

\begin{document}
	\makeform
	\HygieneConcept
	\Abbreviations
\end{document}
```

(Die Formulare an der TH-Köln, aus denen dieses Projekt hervorging, sind mehrsprachig angelegt; 
und dazu wird das LaTeX-Paket [polyglossia](https://ctan.org/pkg/polyglossia?lang=en) verwendet.
Das wiederum erfordert den Einsatz von [XeTeX](https://en.wikipedia.org/wiki/XeTeX).)

## Programme

### Formular-Generator

Das Programm `kkm_form` erzeugt die TeX-Quelldateien (s. o.) und die PDF-Dateien.

### Mailer

Im Programm `form_mailer.py` werden die folgenden Schritte ausgeführt:

1. Der *Collector* (aus dem Modul `collector.py` liest die
   Mitgliederdaten aus der Datenbank und liefert eine Liste mit Namen
   und E-Mail-Adressen (Klasse `Member`).
2. Für jede `Member`-Instanz aus der Liste wird mit Hilfe des Moduls
   `kkm_form.py`ein personenbezogenes Formular erzeugt. Wenn nicht
   anders festgelegt, wird als Datum automatisch der jeweils nächste
   Donnerstag verwendet.
3. Mit Hilfe der
   [`email`](https://docs.python.org/3/library/email.html)-Bibliothek
   wird eine Nachricht an die hinterlegte E-Mail-Adresse mit dem
   Formular als Anhang erzeugt.
4. Die Nachricht wird mit der
   [`smtplib`](https://docs.python.org/3/library/smtplib.html)-Bibliothek
   über ein [Posteo](https://posteo.de)-Konto versendet. Die
   Verbindung wird dabei über
   [`ssl`](https://docs.python.org/3/library/ssl.html) gesichert.
